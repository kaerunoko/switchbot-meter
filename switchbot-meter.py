import yaml
import sys
import binascii
from bluepy.btle import Scanner, DefaultDelegate
import requests

configFile = sys.argv[1]
print('config file: ' + configFile)

with open(configFile, 'r') as file:
  config = yaml.load(file, Loader=yaml.SafeLoader)

  macaddr = config['macaddr']
  form_url = config['form']['url']
  temperature_field = config['form']['temperature_field']
  humidity_field = config['form']['humidity_field']

def post(temperature, humidity):
  requests.post(form_url, data={temperature_field: temperature, humidity_field: humidity})

class ScanDelegate( DefaultDelegate ):
  def __init__( self ):
    DefaultDelegate.__init__( self )

  def handleDiscovery( self, dev, isNewDev, isNewData ):
    if dev.addr == macaddr:
      for ( adtype, desc, value ) in dev.getScanData():
        if ( adtype == 22 ):
          servicedata = bytes.fromhex( value[4:] )
          battery = servicedata[2] & 0b01111111
          isTemperatureAboveFreezing = servicedata[4] & 0b10000000
          temperature = ( servicedata[3] & 0b00001111 ) / 10 + ( servicedata[4] & 0b01111111 )
          if not isTemperatureAboveFreezing:
            temperature = -temperature
          humidity = servicedata[5] & 0b01111111

          isEncrypted            = ( servicedata[0] & 0b10000000 ) >> 7
          isDualStateMode        = ( servicedata[1] & 0b10000000 ) >> 7
          isStatusOff            = ( servicedata[1] & 0b01000000 ) >> 6
          isTemperatureHighAlert = ( servicedata[3] & 0b10000000 ) >> 7
          isTemperatureLowAlert  = ( servicedata[3] & 0b01000000 ) >> 6
          isHumidityHighAlert    = ( servicedata[3] & 0b00100000 ) >> 5
          isHumidityLowAlert     = ( servicedata[3] & 0b00010000 ) >> 4
          isTemperatureUnitF     = ( servicedata[5] & 0b10000000 ) >> 7

          print( '----' )
          print( 'battery: '     + str( battery ) )
          print( 'temperature: ' + str( temperature ) )
          print( 'humidity: '    + str( humidity ) )
          print( '' )
          print( 'isEncrypted: '            + str( bool( isEncrypted ) ) )
          print( 'isDualStateMode: '        + str( bool( isDualStateMode ) ) )
          print( 'isStatusOff: '            + str( bool( isStatusOff ) ) )
          print( 'isTemperatureHighAlert: ' + str( bool( isTemperatureHighAlert ) ) )
          print( 'isTemperatureLowAlert: '  + str( bool( isTemperatureLowAlert ) ) )
          print( 'isHumidityHighAlert: '    + str( bool( isHumidityHighAlert ) ) )
          print( 'isHumidityLowAlert: '     + str( bool( isHumidityLowAlert ) ) )
          print( 'isTemperatureUnitF: '     + str( bool( isTemperatureUnitF ) ) )
          print( '----' )
          post( str( temperature ), str( humidity ) )

          exit()

scanner = Scanner().withDelegate( ScanDelegate() )
scanner.scan( 0 )
