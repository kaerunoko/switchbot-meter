# Switchbot to Google Spreadsheet

## install
This tool depends on bluepy and only works at Linux.

### install packages
```sh
pip install -r requirements.txt
```

### set config file
Make config file such as `room1.yaml`.

```yaml
# Your switchbot mac address
macaddr: 'aa:bb:cc:dd:ee:ff'
# Google Form for posting log
form:
  url: 'https://docs.google.com/forms/u/0/d/e/{id}/formResponse'
  temperature_field: 'entry.11111111'
  humidity_field: 'entry.2222222'
```

#### device mac address
You can find your SwitchBot mac address in smartphone SwitchBot app.

#### Google SpreadSheet
1. create new Google Form
2. insert two text fields, such as 'temperature' and 'humidity'
3. copy prefill link and get filed ids

## execute
```sh
python ./switchbot-meter.py ./room1.yaml
```

## automation
### add cron as root
```
*/5 * * * * /home/pi/.pyenv/versions/3.6.8/bin/python /home/pi/switchbot/switchbot-meter.py /home/pi/switchbot/room1.yaml >> /tmp/switchbot.log 2>&1
```

*Only root user can connect Bluetooth*

## LICENSE
MIT License.

I copied sample code from https://qiita.com/warpzone/items/11ec9bef21f5b965bce3
